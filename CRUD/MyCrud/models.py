from django.db import models


# Create your models here.
class Post(models.Model):
    title = models.IntegerField()
    species = models.CharField(max_length=120, default='monkey', help_text="e.g. monkey")
    somasurface = models.CharField(max_length=120, null=True)
    pngurl = models.CharField(max_length=600, null=True)
    nname = models.CharField(max_length=500, null=True)

    def __str__(self):
        return self.title


class PostTest:
    title = "What's up"
    species = "monkey"
    somasurface = "test"
    pngurl = ""

    def __str__(self):
        return self.title

    def gettitle(self):
        return self.title


