from django.shortcuts import render, redirect, get_object_or_404
from .forms import PostForm
from .models import Post
from django.views.generic import ListView, DetailView, TemplateView
import json
import urllib.request


# Create your views here.
# home view for posts. Posts are displayed in a list
class IndexView(ListView):
    template_name = 'Crud/index.html'
    context_object_name = 'post_list'

    def get_queryset(self):
        saveifneeded()
        return Post.objects.all()


# Detail view (view post detail)
class PostDetailView(DetailView):
    model = Post
    template_name = 'Crud/post-detail.html'


# New post view (Create new post)
def postview(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('index')
    form = PostForm()
    return render(request, 'Crud/post.html', {'form': form})


# Edit a post
def edit(request, pk, template_name='Crud/edit.html'):
    post = get_object_or_404(Post, pk=pk)
    form = PostForm(request.POST or None, instance=post)
    if form.is_valid():
        form.save()
        return redirect('index')
    return render(request, template_name, {'form': form})


# Delete post
def delete(request, pk, template_name='Crud/confirm_delete.html'):
    post = get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        post.delete()
        return redirect('index')
    return render(request, template_name, {'object': post})


def saveifneeded():
    parsed = json.load(urllib.request.urlopen("http://neuromorpho.org/api/neuron/select?q=species:monkey"))
    arr = []
    for i in range(50):
        species = parsed["_embedded"]["neuronResources"][i]["species"]
        neuron_id = parsed["_embedded"]["neuronResources"][i]["neuron_id"]
        soma_surface = parsed["_embedded"]["neuronResources"][i]["soma_surface"]
        png_url = parsed["_embedded"]["neuronResources"][i]["png_url"]
        nname = parsed["_embedded"]["neuronResources"][i]["neuron_name"]
        Post.objects.get_or_create(title=neuron_id, species=species, somasurface=soma_surface, pngurl=png_url, nname=nname)